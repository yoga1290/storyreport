const fs = require('fs');
const {
  oauthRefreshTokenExchange, // xchange refresh w access
  mapDriveFilesFromConfigToStreamURL,
  uploadFile // upload final output
} = require('./gSvc.js')

const h5recorder = require('h5recorder')

let queue = []
let alreadyRunning = false;

function execEntry({ refreshToken, h5RConfig }, callback) {
  oauthRefreshTokenExchange(refreshToken, (accessToken) => {

    mapDriveFilesFromConfigToStreamURL(h5RConfig, accessToken, (newH5RConfig) => {
        console.log('mapDriveFilesFromConfigToStreamURL', JSON.stringify(newH5RConfig));

        h5recorder(newH5RConfig).then((outputVideo) => {
          console.log('TODO', outputVideo)

          uploadFile(accessToken, outputVideo, (err, response, body)=> {
            console.log('uploadFile', err, body);
            fs.unlinkSync(outputVideo);
          })
          callback()

        }, (err) => {
          console.log('TODO2', err)
          callback()
          throw err
          process.exit(1)
        })
    })

  })
}

function runAsync() {
  if (queue.length > 0) {

    alreadyRunning = true
    let it = queue.shift() //dequeue
    execEntry(it, runAsync) // recurse on callback

  } else { // empty queue
    alreadyRunning = false
  }
}

module.exports = {

  queue(refreshToken, h5RConfig) {
    queue.push({ refreshToken, h5RConfig })

    if(!alreadyRunning) {
      runAsync()
    }
  }

}

const express = require('express');
const h5recorder = require('h5recorder')
// https://expressjs.com/en/resources/middleware/multer.html
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

const { gSvc, QueueSvc } = require('./services')
const {
  getLoginURL,
  postFilesToDrive,
  oauthCodeExchange,
  streamFileFromDrive,
  postFilesToLocalDisk,
  mapDriveFilesToEntries,
  mapDriveFilesToAudioEntries,
  mapLocalFilesToEntries,
  mapLocalFilesToAudioEntries
} = gSvc

const { origins, exitTokenPage } = require('./config')


const PROD = (process.env.NODE_ENV === 'production')

var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", origins);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

console.log('process.env.NODE_ENV', process.env.NODE_ENV)

app.set('port', (process.env.PORT || 5000));

app.get('/drive/login', (req, res, next) => {
  if (PROD) {
    let url = getLoginURL('NIY')
    res.redirect(url)
  } else {
    res.redirect(`${exitTokenPage}?access_token=a&refresh_token=b`)
  }
})

// note accessToken will be appended sometime after being exchanged w the refresh token in QueueSvc
app.get('/drive/stream/:fileId/:accessToken', streamFileFromDrive);

app.post('/drive/upload', (req, res, next) => {
  console.log('/drive/upload')
  let { access_token = '', refresh_token = '' } = req.query

  if (PROD) {
    postFilesToDrive(access_token, req, (driveFileIds, audioDriveFileIds, reqBody) => {
      console.log('END OF REQUEST', driveFileIds, audioDriveFileIds, reqBody);
      let h5RConfig = JSON.parse(reqBody.data)

      h5RConfig = mapDriveFilesToEntries(h5RConfig, driveFileIds)
      h5RConfig = mapDriveFilesToAudioEntries(h5RConfig, audioDriveFileIds)

      res.send({ done: 'ok', h5RConfig })

      QueueSvc.queue(refresh_token, h5RConfig);
    })
  } else {
    postFilesToLocalDisk(req, (localFileIds, audioLocalFileIds, reqBody) => {
      console.log('END OF REQUEST', localFileIds, audioLocalFileIds, reqBody);
      let h5RConfig = JSON.parse(reqBody.data)
      h5RConfig = mapLocalFilesToEntries(h5RConfig, localFileIds)
      h5RConfig = mapLocalFilesToAudioEntries(h5RConfig, audioLocalFileIds)

      h5recorder(h5RConfig).then((output)=>{
          console.log('respo');
          res.end(JSON.stringify({output}));
          //TODO: strem file may be for localhost
      }, (err) =>{
          console.log('error', err);
          throw error;
      })
    })
  }

})

app.get('/_oauth', (req, res) => {

    console.log('param:', req.query)
    let code = req.query.code
    let state = req.query.state

    console.log(code, state);
    oauthCodeExchange(code, (body) => {
      console.log('body.access_token', body.access_token)
      let {access_token, refresh_token} = body
      console.log('refresh_token', refresh_token)
      //TODO: store token on the server, no?
      console.log('exitTokenPage', exitTokenPage)
      //TODO redirect
      res.send(`<script>
        window.location.href="${exitTokenPage}?access_token=${access_token}&refresh_token=${refresh_token}";
        setTimeout(function(){ window.close(); }, 20000);
      </script>`)
    })
});

const server = app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

server.timeout = 5 * 60 * 1000

module.exports = {
  app
}
